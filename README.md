# Admin Menu Levels

Admin Menu Levels provides two extra options on menu administration pages (admin/structure/menu/manage/{menu_name}):

1. A select list from which the user can choose the depth of the menu items to display (inclusive — meaning if "Two" is selected, menu items of depth 1 AND 2 will be shown)
2. A checkbox that will show/hide disabled menu items, respecting the choice made in the select list above

This helps with menus that can often be very long, with tens or hundreds of menu items in the list — the administration menu, for instance; it filters the list so you see only those items that you are interested in.

## Installation and use
1. `composer require 'drupal/admin_menu_levels:^1.0'`
2. `drush:en admin_menu_levels`
3. Browse to admin/structure/menu/manage/{menu_name}
4. Select from the select list labeled "Number of levels to display" and/or check the checkbox labeled "Hide disabled items"
