# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.0.2] - 2022-09-21
### Fixed
- Fix empty tagged release

## [1.0.1] - 2022-09-21
### Fixed
- 3336824: Use val() instead of textContent (credit to [Shriaas](https://www.drupal.org/u/shriaas))

## [1.0.x] - 2022-09-21
### Added
- All initial files, including `CHANGELOG`, `README`, `LICENSE`, `composer.json`, `admin_menu_levels.info.yml`, `admin_menu_levels.libraries.yml`, `admin_menu_levels.module`, `admin-menu-levels.es6.js`, and `admin-menu-levels.js`.

[Unreleased]: https://git.drupalcode.org/project/admin_menu_levels/-/compare/1.0.2...1.0.x
[1.0.2]: https://git.drupalcode.org/project/admin_menu_levels/-/tags/v1.0.2
[1.0.1]: https://git.drupalcode.org/project/admin_menu_levels/-/tags/v1.0.1
[1.0.0]: https://git.drupalcode.org/project/admin_menu_levels/-/tags/v1.0.0
