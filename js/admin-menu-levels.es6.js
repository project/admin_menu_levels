/**
 * @file
 * Admin menu levels behaviors.
 */
(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.adminMenuLevels = {
    attach: function (context, settings) {
      $(`#menu-edit-form`, context).each(function() {
        const level_select = $(`.js-form-item-field-levels #edit-field-levels`);
        let level_choice = level_select.val();
        const all_items = $(`#menu-overview tbody > tr`);
        const disabled_items_checkbox = $(`#edit-field-enabled-toggle`);
        let hide_disabled_items = disabled_items_checkbox.is(`:checked`);
        all_items.each(function() { // add classes to all items to indicate level in menu tree
          if ($(this).find(`> td .indentation`).length === 0) {
            $(this).addClass(`admin-menu-levels--1`);
          } else if ($(this).find(`> td .indentation`).length === 1) {
            $(this).addClass(`admin-menu-levels--2`);
          } else if ($(this).find(`> td .indentation`).length === 2) {
            $(this).addClass(`admin-menu-levels--3`);
          } else {
            $(this).addClass(`admin-menu-levels`);
          }
        });
        tr_toggle_choice(level_choice); // use initial choice on load to show/hide items
        if (hide_disabled_items) { // use initial choice on load to show/hide items
          all_items.filter(`.menu-disabled`).hide();
        }
        level_select.change(function() { // toggle on change
          level_choice = level_select.val();
          tr_toggle_choice(level_choice);
        });
        disabled_items_checkbox.change(function() { // toggle on change
          hide_disabled_items = disabled_items_checkbox.is(`:checked`);
          tr_toggle_choice(level_choice);
        });
        function tr_toggle_choice(choice) { // show/hide <tr>s based on user selection
          all_items.hide(); // start with a clean slate by hiding everything
          switch(choice) { // then selectively show what user wants to see
            case "1":
              hide_disabled_items ?
                all_items.filter(`.menu-enabled`).filter(`.admin-menu-levels--1`).show() :
                all_items.filter(`.admin-menu-levels--1`).show();
              break;
            case "2":
              hide_disabled_items ?
                all_items.filter(`.menu-enabled`).filter(`.admin-menu-levels--1, .admin-menu-levels--2`).show() :
                all_items.filter(`.admin-menu-levels--1, .admin-menu-levels--2`).show();
              break;
            case "3":
              if (hide_disabled_items) {
                all_items.filter('.menu-enabled').filter(`.admin-menu-levels--1, .admin-menu-levels--2, .admin-menu-levels--3`).show();
              } else {
                all_items.filter(`.admin-menu-levels--1, .admin-menu-levels--2, .admin-menu-levels--3`).show();
              }
              break;
            case "all":
              if (hide_disabled_items) {
                all_items.filter(`.menu-enabled`).show();
              } else {
                all_items.show();
              }
              break;
          }
        }
      });
    }
  };
} (jQuery, Drupal));
